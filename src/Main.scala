import scala.io.StdIn.readLine

object Main {

  def main(args:Array[String]): Unit = {
    all(2)
  }

  def alterandoConstante(nome:String): Unit = {
    //nome = "Vieira"
    printf(nome)
  }

  def exibirNome(): Unit = {
    val nome:String = readLine("Coloque seu primeiro nome: ")
    val sobreNome:String = readLine("Coloque seu sobrenome: ")

    println(s"$nome $sobreNome")

  }

  def somarNumeros(valor: Int): Int = {
    val soma = valor + valor
    soma
  }

  def multiplicarNumero(valor: Int): Int = {
    valor * 2
  }

  def altaOrdemMultiplicarLista(valor: Int): List[Int] = {
    val lista = List(2, 4, 6, 8, 10)

    lista.map(valor * _)
  }

  def all(valor: Int): List[Int] = {
    val numeros = List(1,2,3,4,5,6,7,8,9,10)

    val numerosPares = numeros.filter(_ % 2 == 0)
    println("Os numeros pares: " + numerosPares)

    println("Os numeros impares: " + numeros.filterNot(numerosPares.contains(_)))
    numeros
  }
}
